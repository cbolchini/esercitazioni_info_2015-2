#include <stdio.h>

#define M_RAD3_4 0.433
#define M_PI 3.14

int main(int argc, char *argv[])
{
    float d;
    float aq, ac, at;
    float r;

    printf("Area calculator\n\n");

    printf("Insert the real number D: ");
    scanf("%f", &d);

    /* square area*/
    aq = d*d;

    /* circle area */
    r = d/2;
    ac = M_PI*(r*r);

    at = M_RAD3_4*(d*d);

    printf("\n");
    printf("The calculated areas are:\n");
    printf("Square of side %f = %f\n", d, aq);
    printf("Circle of diameter %f = %f\n",d, ac);
    printf("Equilateral triangle of side %f = %f\n", d, at);
    return 0;
}
