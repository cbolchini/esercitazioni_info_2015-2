#include <stdio.h>

int main(int argc, char *argv[])
{
    float a, b;
    float x;

    printf("One variable linear equation solver\n");
    printf("Equation in the form ax + b = 0\n");

    printf("Insert a: ");
    scanf("%f", &a);

    printf("Insert b: ");
    scanf("%f", &b);

    if( a != 0 ){
        x = - b/a;
        printf("The solution is x = %f\n", x);
    }else{
        /* CASE a==0 */
        if( b==0 )
        {
            printf("Equation admits infinitely many solutions!\n");
        }else{
            printf("No possible solutions!\n");
        }
    }

    return 0;
}
