#include <stdio.h>

int main(int argc, char *argv[])
{
    int a,b; // input variables
    int c; // temp var

    printf("insert two numbers a and b\n");
    scanf("%d %d", &a, &b);

    /* exchange the numbers */
    c = b;
    b = a;
    a = c;

    /* print the results */

    printf("a: %d, b: %d\n", a, b) ;
    return 0;
}
