#include <stdio.h>

#define LUNG_MAX 60
#define SENT 0

int main(int argc, char *argv[]){
        int list[LUNG_MAX];
        int i,k;
        int lung;
        int inp;
        int aux;


        i = 0;
        scanf("%d", &inp);
        while(inp != SENT){
                list[i] = inp;
                i++;
                scanf("%d", &inp);
        }
        lung = i;

        /* ordina i valori acquisiti */
        for ( i = 0; i < lung-1; i++){
                for ( k = i+1; k < lung; k++){
                        if ( list[k] < list[i] ){
                                aux = list[i];
                                list[i] = list[k];
                                list[k] = aux;
                        }
                }
        }

        /* otteni le coppie */
        for ( i = 0; i < lung; i++){
                        for ( k = i+1; (k<lung && list[k]!=list[i]); k++){
                                if ( list[k-1] != list[k] ){
                                        printf("%d,%d\n",list[i],list[k]);
                                }
                        }
        }

        return 0;
}

