#include <stdio.h>

#define B10 10
#define B2  2

int main(int argc, char *argv[])
{
    int bin, ndec, r, n, pown;

    printf("Inserisci un numero intero compreso nell'intervallo [0, 2047]: ");
    do{
        scanf("%d", &ndec);
    }while(ndec < 0 || ndec > 2047);
    bin = 0;
    n = ndec;
    pown = 1;

    while(n > 0){
        r = n % B2;
        n = n / B2;
        bin = bin + r*pown;
        pown = pown*B10;
    }



    printf("%d = %d\n", ndec, bin);

    return 0;
}
