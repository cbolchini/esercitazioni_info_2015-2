#define MAX 10

int main(int argc, char *argv[]) {
  int M, N, Q, r, c, k, sum = 0;
  int prima[MAX][MAX], seconda[MAX][MAX], moltiplicata[MAX][MAX];

  printf("Numero righe e colonne prima matrice (max %d)\n",MAX);
  do {
    scanf("%d%d", &M, &N);
  } while( N>MAX || M > MAX);

  printf("Elementi prima:\n");

  for (  r = 0 ; r < M ; r++ )
    for ( c = 0 ; c < N ; c++ )
      scanf("%d", &prima[r][c]);

  do {
    printf("Numero colonne seconda matrice (max %d)\n",MAX);
    scanf("%d", &Q);
  } while ( Q > MAX );

   printf("Elementi secondaa:\n");

    for ( r = 0 ; r < N ; r++ )
      for ( c = 0 ; c < Q ; c++ )
        scanf("%d", &seconda[r][c]);

    for ( r = 0 ; r < M ; r++ ) {
      for ( c = 0 ; c < Q ; c++ ) {
        for ( sum = 0, k = 0 ; k < N ; k++ ) {
          sum = sum + prima[r][k]*seconda[k][c];
        }

        moltiplicata[r][c] = sum;
      }
    }

    printf("Prodotto:\n");

    for ( r = 0 ; r < M ; r++ ) {
      for ( c = 0 ; c < Q ; c++ ) {
        printf("%d\t", moltiplicata[r][c]);
      }
      printf("\n");
    }

  return 0;
}
