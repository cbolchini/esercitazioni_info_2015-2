#define DIM 2

#include <stdio.h>

int main(int argc, char *argv[]){
    int A[DIM][DIM];
    int i, j;
    int det;

    /* acquire matrix */
    for(i=0;i<DIM;i++)
        for(j=0;j<DIM;j++)
            scanf("%d", &A[i][j]);

    det = A[0][0]*A[1][1] - A[0][1]*A[1][0];

    printf("risultato: %d\n", det);

    return 0;
}
