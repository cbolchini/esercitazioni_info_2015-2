#include <stdio.h>

#define DIM 10

int main(int argc, char *argv[])
{
    int m[DIM][DIM];

    int i, j, d;
    int flag;

    //acquisizione dimensione effettiva
    do {
        printf("inserire il numero di righe e colonne\n");
        scanf("%d", &d);
    } while (d < 0 || d > d);

    //lettura valori
    for (i = 0; i < d; i++) {
        for (j = 0; j < d; j++) {
            printf("m[%d][%d] = ", i, j);
            scanf("%d", &m[i][j]);
        }
    }

    flag = 0;

    for (i = 0; (i < d) && (!flag); i++){
        for (j = 0; (j < d) && (!flag); j++){

            if ((i != j) && (m[i][j] != 0))
                flag = 1;
        }
    }

    if (flag)
        printf("non ");

    printf("diagonale\n");

    return 0;
}
