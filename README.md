# Esercitazioni Fondamenti di Informatica 2015

Ogni directory contiene il testo e le soluzioni della esercitazione relativa.

per scaricare i file e' possibile:

-  scaricare i singoli file:
    visitare il [sorgente del repository](https://bitbucket.org/oceank/esercitazioni_info_2015/src), aprire il file e utilizzare il pulsante "Raw"
-  [scaricare](https://bitbucket.org/oceank/esercitazioni_info_2015/downloads) l'intero contenuto del repository.
-  clonare e tenere aggiornato il repository tramite il programma git (http://rogerdudler.github.io/git-guide/)
    *  ```git clone git@bitbucket.org:oceank/esercitazioni_info_2015.git``` per scaricare una copia locale
    *  ```git pull``` all'interno della directory creata dal precedente comando per aggiornarla

