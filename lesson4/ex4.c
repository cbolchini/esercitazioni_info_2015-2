#include <stdio.h>

#define  MAXDIM 100;

int main(void)
{
    /* dimensione massima stringa di caratteri */
    char frase[MAXDIM +1];
    char nuovafrase[MAXDIM +1];
    int lung_stringa;
    int i;
    /* LEGGI LA FRASE INSERITA DA TASTIERA */

    printf ("Inserisci una frase di al massimo %d caratteri: ", MAXDIM);
    gets(frase);
    /* CALCOLA LA LUNGHEZZA DELLA FRASE */
    for(i=0; lung_stringa[i]!='\0'; i++);
    lung_stringa = i;


    /* STAMPA LA FRASE INSERITA */
    printf("La frase inserita e’: ");
    printf("%s", frase);
    printf("La frase contiene %d caratteri (inclusi gli spazi)\n", lung_stringa);

    /* COSTRUISCI LA NUOVA FRASE */
    for ( i=0; i<lung_stringa; i++ )
    {
    /* IL CARATTERE "frase[i]" E’ LA PRIMA LETTERA DI UNA PAROLA SE IL
     * CARATTERE PRECEDENTE ("frase[i-1]") ERA UNO SPAZIO OPPURE SE E’ IL PRIMO
     * CARATTERE DELLA FRASE (OSSIA i==0). IN QUESTO CASO IL CARATTERE "frase[i]"
     * E’ CONVERTITO IN CARATTERE MAIUSCOLO. IN TUTTI GLI ALTRI CASI IL CARATTERE
     * "frase[i]" E’ CONVERTITO IN CARATTERE MINUSCOLO */
        if ( (i==0) || isspace(frase[i-1]) )
            if(frase[i] < 'z' && frase[i] > 'a')
                nuovafrase[i] = frase[i] - 'a' + 'A';
        else
            if(frase[i] < 'Z' && frase[i] > 'A')
                nuovafrase[i] = frase[i] - 'A' + 'a';
    }
    nuovafrase[lung_stringa] = ’\0’;
    /* STAMPA LA FRASE MODIFICATA */
    printf("La frase modificata e’: ");
    printf("%s", nuovafrase)"

    return 0;
}
